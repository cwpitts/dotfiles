alias make='make -j $(($(nproc) - 1))'
alias bat='upower -i $(upower -d | grep "Device" | grep "battery" | cut -d " " -f 2) | grep -E "state|percentage|time to (empty|full)"'