export PYTHONSTARTUP=~/.pythonrc
export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWUNTRACKEDFILES=true
export PATH=${PATH}:/usr/local/go/bin:~/go/bin:~/anaconda3/bin:~/.local/flutter/bin:~/.pub-cache/bin:/home/chris/.gem/ruby/2.7.0/bin
export EDITOR=emacs
