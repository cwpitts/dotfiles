import os
import sys
from importlib import reload
from pathlib import Path

# Prompt for Python interpreter (not used by IPython)
sys.ps1 = u"\u03BB\u03BB\u03BB "

def execfile(path: Path):
    """Execute the given file.

    Parameters
    ----------
    """
    fname: str = path.stem
    with open(path) as f:
        c = compile(f.read(), fname, "exec")
    exec(c)
