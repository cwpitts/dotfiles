import importlib
from shutil import which
from typing import Any

from IPython.core.magic import needs_local_scope, register_line_magic


@needs_local_scope
@register_line_magic
def mylab(line: str, local_ns: dict[str, Any]) -> None:
    # Import useful modules/functions
    local_ns["np"] = importlib.import_module("numpy")
    local_ns["pd"] = importlib.import_module("pandas")
    local_ns["mpl"] = importlib.import_module("matplotlib")
    local_ns["plt"] = importlib.import_module("matplotlib.pyplot")
    local_ns["Path"] = getattr(importlib.import_module("pathlib"), "Path")
    local_ns["reload"] = importlib.reload

    # Nice default plots
    plt.style.use("bmh")
    plt.ion()
    mpl.rcParams["text.usetex"] = True if which("lualatex") is not None else False

    print("Imported NumPy, Pandas, Matplotlib")
    print(f"Plotting style set to 'bmh', usetex={mpl.rcParams['text.usetex']}")

del mylab
