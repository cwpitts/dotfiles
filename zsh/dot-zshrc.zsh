# If you come from bash you might have to change your $PATH.
export PATH=${HOME}/.local/bin:${HOME}/bin:/usr/local/bin:${PATH}:/bin:/sbin:${HOME}/.local/share/gem/ruby/3.0.0/bin:${HOME}/.dotnet/tools

# Path to your oh-my-zsh installation.
export ZSH="/home/chris/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
# ZSH_THEME="candy"
ZSH_THEME="powerlevel10k/powerlevel10k"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    git
    gitfast
    zsh-autosuggestions
    zsh-completions
    gitcd
    rust
		fast-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
fpath=(~/.zsh $fpath)
#autoload -Uz compinit && compinit
#zstyle ':completion:*:*:git:*' script ~/.zsh/git-completion.bash
zstyle ':completion:*:*:make:*' tag-order 'targets'

alias zshconfig="emacs ~/.zshrc"
alias ohmyzsh="emacs ~/.oh-my-zsh"
alias make='make -j $(($(nproc) - 1))'
alias bat='upower -i $(upower -d | grep "Device" | grep "battery" | cut -d " " -f 2) | grep -E "state|percentage|time to (empty|full)"'

export PYTHONSTARTUP=~/.pythonrc
export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWUNTRACKEDFILES=true
# Go PATH locations
export PATH=${PATH}:/usr/local/go/bin:~/go/bin
# Anaconda PATH locations
export PATH=${PATH}:~/anaconda3/bin
# Dart/Flutter PATH locations
export PATH=${PATH}:~/.local/flutter/bin:~/.pub-cache/bin
# Ruby
export PATH=${PATH}:/home/chris/.gem/ruby/2.7.0/bin
# Rust/Cargo
export PATH=${PATH}:~/.cargo/bin

export EDITOR=emacs
export ANDROID_SDK_ROOT=~/Android/Sdk
export QT_STYLE_OVERRIDE=fusion
export GOPATH=~/go

# Key bindings
bindkey -r "^[l"
bindkey "^[l" down-case-word

# # ex - archive extractor
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# better yaourt colors
export YAOURT_COLORS="nb=1:pkg=1:ver=1;32:lver=1;45:installed=1;42:grp=1;34:od=1;41;5:votes=1;44:dsc=0:other=1;35"
load_fmt_success='\e[0;49;93m%-20s \e[0;49;39m: \e[0;49;32m%-5s\e[0;49;39m\n'
load_fmt_failure='\e[0;49;93m%-20s \e[0;49;39m: \e[0;49;91m%-5s\e[0;49;39m\n'

# Load env and alias files
if [ -e ${HOME}/anaconda3/etc/profile.d/conda.sh ]
then
    source ${HOME}/anaconda3/etc/profile.d/conda.sh
    printf "${load_fmt_success}" "Anaconda Python" "LOADED"
else
    printf "${load_fmt_failure}" "Anaconda Python" "NOT LOADED"
fi

if which colorls 2>&1 >/dev/null
then
    alias ls=colorls
fi

function cd()
{
    if builtin cd "${@}"
    then
        ls
    fi
}

function up()
{
    levels=${1:-1}

    re='^[0-9]+$'
    if ! [[ ${levels} =~ ${re} ]]
    then
        printf "Error: Not an integer\n"
        return 1
    fi

    while [[ ${levels} > 0 ]]
    do
        builtin cd ..
        ((levels -= 1))
    done
}

if [ -e ~/.fortunes/scifi.fortunes.dat ]
then
    fortune ~/.fortunes/scifi.fortunes
fi

unsetopt AUTO_CD

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

source /home/chris/.config/broot/launcher/bash/br
