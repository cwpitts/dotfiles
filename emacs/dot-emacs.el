;;; package -- summary
;;; Commentary:
;;; This is my .emacs file
;;; Code:

(setenv "TZ" "America/Denver")

;; Custom packages
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("elpa" . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("elpa-nongnu" . "https://elpa.nongnu.org/nongnu/"))

(when (not (package-installed-p 'use-package))
  (package-refresh-contents)
  (package-install 'use-package))
(setq use-package-compute-statistics t)
(eval-when-compile
  (require 'use-package))

(setq package-selected-packages
      '(beacon
				company
        diff-hl
        doom-themes
        ef-themes
        org-kanban
        org-ql
        poet-theme
        posframe
        web-mode
        all-the-icons
        blamer
        cargo
        centaur-tabs
        clang-format
        cmake-mode
        dashboard
        doom-modeline
        flycheck
        flycheck-pycheckers
        flycheck-rust
        flyspell
        flyspell-correct
        go-mode
        goto-line-preview
        helm
        helm-dash
        jinja2-mode
        lsp-mode
        magit
        markdown-mode
        markdown-preview-mode
        neotree
        org
        org-caldav
        org-roam
        posframe
        pyvenv-mode
        rainbow-mode
        rust-mode
        yasnippet
        shell-pop))

(setq emacs-config-path (file-truename load-file-name))
(setq emacs-config-dir (file-name-directory emacs-config-path))
(setq cwpitts-path (expand-file-name "cwpitts.el" emacs-config-dir))

(when (file-exists-p cwpitts-path)
  (load-file cwpitts-path))

(when (file-exists-p (file-truename "~/.emacs_secrets.el"))
  (load-file "~/.emacs_secrets.el"))

(defconst defer-timer 5)

(let ((default-directory emacs-config-dir))
  (normal-top-level-add-subdirs-to-load-path))

;; screenshot
(when (package-installed-p 'posframe)
      (use-package screenshot))

;; shell-pop
(use-package shell-pop
  :defer defer-timer
  :init
  (setq shell-pop-autocd-to-working-dir t)
  (setq shell-pop-term-shell "/bin/zsh")
  :bind
  (("<f7>" . shell-pop)))

(use-package beacon
  :config
  (beacon-mode))

(use-package doom-modeline
  :init
  (doom-modeline-mode))

;; vc
(use-package vc
  :defer defer-timer
  :init
  (setq vc-follow-symlinks t))

;; flex
(use-package flex)

;; lsp
(use-package lsp
  :defer defer-timer
  :init
  (setq lsp-enable-dap-auto-configure nil)
  (setq lsp-java-java-path "/usr/lib/jvm/java-11-openjdk/bin/java")
  (setq gc-cons-threshold 100000000)
  (setq read-process-output-max (* 1024 1024))
  (setq lsp-idle-delay 0.500)
  (setq lsp-log-io nil)
  :hook
  (c++-mode c-mode rust-mode go-mode LaTeX-mode))

;; yas-minor-mode
(use-package yas-minor-mode
  :hook lsp-mode)

;; flycheck
(use-package flycheck
  :init
  (setq flycheck-python-pycompile-executable "python3")
  (setq flycheck-gcc-language-standard "c11")
  :config
  (flycheck-define-checker
      python-mypy ""
      :command ("mypy"
                "--ignore-missing-imports"
                "--python-version" "3.7"
                source-original)
      :error-patterns
      ((error line-start (file-name) ":" line ": error:" (message) line-end))
      :modes python-mode)
  (add-to-list 'flycheck-checkers 'python-mypy t)
  (flycheck-add-next-checker 'python-pylint 'python-mypy t)
  :hook
  '(python c++ c elisp ruby go rust lisp elisp sh))

;; neotree
(use-package neotree
  :init
  (setq neo-window-fixed-size nil)
  (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
  (global-set-key [f8] 'neotree-toggle)
  :config
  (add-hook 'neo-enter-hook #'cwpitts/neotree--fit-window)
  (mapc (lambda (regex)
          (add-to-list 'neo-hidden-regexp-list regex))
        (list "__pycache__"
              "\\.aux"
              "\\.bbl"
              "\\.blg"
              "\\.o"
              "\\.bcf"
              "\\.log")))

;; magit
(use-package magit
  :bind
  (("C-x g" . magit-status)))

;; flyspell
(use-package flyspell
  :demand
  :after
  (org)
  :bind
  (:map flyspell-mode-map
        ("C-;" . flyspell-popup-correct))
  :config
  (add-hook 'org-mode-hook #'flyspell-mode)
  (add-hook 'latex-mode-hook #'flyspell-mode))
(use-package flyspell-correct
  :init
  (setq flyspell-issue-message-flag nil)
  (setq flyspell-popup-correct-delay 1.5)
  :config
  (add-hook 'flyspell-mode-hook #'flyspell-popup-auto-correct-mode))

;; jinja-2
(use-package jinja2-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.j2\\'" . jinja2-mode)))

;; helm
(use-package helm
  :config
  (helm-mode)
  ; Why are we not using :bind? Because this config can bootstrap
  ; itself. The helm package won't be installed before the first
  ; bootstrap, so the M-x command will be unusable if the bind is
  ; unconditional, which it is in use-package's :bind argument. The
  ; cwpitts/bootstrap function reloads the config, which will trigger
  ; the conditional in the when form.
  (when (package-installed-p 'helm)
    (bind-keys ("M-x" . helm-M-x)
               ("C-h C-l" . helm-locate)
               ("C-h C-g" . helm-ls-git-ls))))

(use-package helm-dash
  :defer defer-timer
  :init
  (setq-local helm-dash-docsets
              '("Python_3" "NumPy" "SciPy" "Jinja")))

;; Centaur tabs
(use-package centaur-tabs
  :demand
  :init
  (setq centaur-tabs-style "bar")
  (setq centaur-tabs-set-bar 'left)
  (setq centaur-tabs-set-icons t)
  (setq centaur-tabs-gray-out-icons 'buffer)
  :bind
  (("M-]" . centaur-tabs-forward)
   ("M-[" . centaur-tabs-backward)
   ("C-M-]" . centaur-tabs-move-current-tab-to-right)
   ("C-M-[" . centaur-tabs-move-current-tab-to-left))
  :config
  (centaur-tabs-mode t))

;; rainbow-mode
(use-package rainbow-mode
  :config
  (add-hook 'prog-mode-hook #'rainbow-mode)
  (add-hook 'markdown-mode-hook #'rainbow-mode)
  (add-hook 'web-mode-hook #'rainbow-mode))

;; diff-hl
(use-package diff-hl
  :init
  (add-hook 'prog-mode-hook #'diff-hl-mode)
  (add-hook 'LaTeX-mode-hook #'diff-hl-mode)
  (add-hook 'markdown-mode-hook #'diff-hl-mode)
  (add-hook 'web-mode-hook #'diff-hl-mode))

;; hideshowvis
(load-file (expand-file-name "hideshowvis/hideshowvis.el" emacs-config-dir))
(use-package hideshowvis
  :commands diff-hl-mode
  :bind
  (("C-<tab>" . hs-hide-block)
   ("C-<S-iso-lefttab>" . hs-show-block))
  :init
  (add-hook 'prog-mode-hook #'hideshowvis-enable)
  (add-hook 'prog-mode-hook #'hideshowvis-minor-mode))

;; Handle the Custom variables in a different file
(setq custom-file "~/.emacs.d/custom.el")
(if (file-exists-p "~/.emacs.d/custom.el")
    (load-file "~/.emacs.d/custom.el"))

;; Language settings
;; Python
(setenv "WORKON_HOME" "~/anaconda3/envs")
(add-hook 'python-mode-hook
          (lambda ()
            (add-hook 'before-save-hook
                      'py-isort-buffer nil 'local)))
;; pyvenv
(use-package pyvenv-mode
  :init
  (setq pyvenv-mode-line-indicator
        '(pyvenv-virtual-env-name ("[venv:" pyvenv-virtual-env-name "] ")))
  (setq lsp-pylsp-plugins-jedi-use-pyenv-environment t)
  :hook python-mode)
;; end Python

;; C++
;; Configure tabs to be two spaces in C/C++
(use-package clang-format
  :init
  (setq clang-format-style "file")
  (setq c-default-style "linux")
  (setq c-basic-offset 2)
  :config
  (add-hook
   'c++-mode-hook
   (lambda ()
     (add-hook 'before-save-hook
               #'clang-format-buffer nil 'local))))
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
;; end C++

;; Go
(use-package go-mode
  :init
  (setq godoc-and-godef-command "/usr/local/go/bin/godoc")
  (setq godef-command "~/go/bin/godef")
  (setq gofmt-command "/usr/bin/gofmt")
  (setq company-go-gocode-command "~/go/bin/gocode")
  :config
  (setenv "PATH" (concat "/usr/local/go/bin" ":" "~/go/bin" ":" (getenv "PATH")))
  :bind
  ("M-g M-d" . godoc-at-point))
;; end Go

;; Markdown
(use-package markdown-mode
  :init
  (setq markdown-command "/usr/sbin/pandoc"))
;; end Markdown

;; Web development
(use-package web-mode
  :init
  (setq web-mode-code-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-markup-indent-offset 2))

;; Rust
(use-package rust-mode
  :init
  (setq lsp-rust-server 'rust-analyzer)
  (setq lsp-rust-analyzer-server-command "~/.cargo/bin/rust-analyzer")
  (setq lsp-rust-rls-server-command "~/.cargo/bin/rust-analyzer"))
(use-package cargo)
;; end Rust

;; org-mode
(use-package org
  :init
  (setq org-use-sub-superscripts (quote {}))
  (setq org-agenda-files '("~/org/byui.org" "~/org/home.org" "~/org/projects.org" "~/org/unm.org"))
  (setq org-journal-dir "~/journal")
  (setq org-todo-keywords
        '((sequence "TODO" "DOING" "|" "DONE")))
  (setq org-todo-keyword-faces
        '(("TODO" . org-warning)
          ("DOING" . "blue")))
  (setq org-hide-emphasis-markers t)
  :config
  (plist-put org-format-latex-options :scale 2.5)
  :hook ((org-mode . (lambda ()
                       (add-hook 'before-save-hook 'org-update-all-dblocks nil 'local)
                       (require 'org-ql-search)))))

(use-package org-caldav
  :after org-mode
  :init
  (setq org-caldav-url "https://cloud.cwpitts.com/remote.php/dav/calendars/chris/")
  (setq org-caldav-calendar-id "personal"))

(use-package org-roam
  :init
  (setq org-roam-directory "~/org/org-roam")
  (setq org-roam-v2-ack t)
  (setq org-roam-db-autosync-mode t))

(use-package org-kanban
  :after org-mode
  :init
  (org-kanban/initialize))

(use-package org-ql
  :after org-mode)

(use-package org-appear
  :after org-mode)

(use-package org-modern
  :after org-mode
  :hook
  (org-mode . global-org-modern-mode)
  :init
  (org-modern-table nil))

;; end org-mode

;; LaTeX
(use-package tex-mode
  :config
  (add-hook 'LaTeX-mode-hook 'reftex-mode)
  (add-hook 'LaTeX-mode-hook (lambda () (add-to-list LaTeX-indent-environment-list '("algorithmic" current-indentation)))))

(use-package recentf
  :init
  (setq recentf-exclude (mapcar 'f-canonical (org-agenda-files)))
  :after
  (org))

(use-package all-the-icons)

(use-package dashboard
  :init
  (setq dashboard-banner-logo-title (format "Emacs @ %s" (system-name)))
  (setq dashboard-startup-banner 'logo)
  (setq dashboard-footer-messages '("Do something great today! Preferably involving penguins."))
  (setq dashboard-set-navigator t)
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-center-content t)
  (setq dashboard-footer-icon
        (all-the-icons-octicon "dashboard"
                               :height 1.1
                               :v-adjust -0.05
                               :face 'font-lock-keyword-face))
  (setq dashboard-items '((recents  . 10)
                          (agenda . 10)))
  (setq dashboard-week-agenda t)
  :config
  (dashboard-setup-startup-hook)
  :after
  (all-the-icons))

;; Blamer
(use-package blamer
  :defer defer-timer
  :init
  (setq blamer-prettify-time-p nil)
  :config
  (global-blamer-mode))

;; Compilation mode goodness
;; https://emacs.stackexchange.com/a/336
;; from enberg on #emacs
(use-package compile
  :init
  (setq compilation-scroll-output t)
  (setq compilation-read-command nil)
  :bind
  (("<f5>" . 'compile))
  :config
  (add-hook 'compilation-finish-functions
            (lambda (buf str)
              (if (null (string-match ".*exited abnormally.*" str))
                  ;;no errors, make the compilation window go away in a few seconds
                  (progn
                    (run-at-time
                     "2 sec" nil 'delete-windows-on
                     (get-buffer-create "*compilation*"))
                    (message "No Compilation Errors!"))))))

;; RGB!
;;(load-file "~/projects/rgb.el/rgb.el")
(use-package rgb
  :defer defer-timer
  :functions
  rgb-mode
  :commands
  rgb-mode
  :init
  (setq rgb-device-ids (list "0"))
  (setq rgb-argmap
        (cl-pairlis
         '(org-mode perl-mode python-mode emacs-lisp-mode rust-mode markdown-mode c-mode c++-mode)
         '(((color . "#800080") (mode . "Static")) ;; org-mode
           ((color . "#FF0000") (mode . "Strobe")) ;; perl-mode
           ((color . "#FFFF00") (mode . "Static")) ;; python-mode
           ((color . "#800080") (mode . "Static")) ;; emacs-lisp-mode
           ((color . "#FFA500") (mode . "Static")) ;; rust-mode
           ((color . "#FFC0CB") (mode . "Static")) ;; markdown-mode
           ((color . "#0000FF") (mode . "Static")) ;; c-mode
           ((color . "#0000FF") (mode . "Static")) ;; c++-mode
           )))
  :config
  (rgb-mode))

;; goto-line-preview
(use-package goto-line-preview
  :bind
  (([remap goto-line] . goto-line-preview)))

;; company-mode
(use-package company-mode
  :hook
  emacs-lisp-mode)

;; Theme
(use-package ef-themes
  :config
  (load-theme 'ef-duo-dark t))

(use-package hass
  :defer defer-timer
  :init
  (setq hass-host "home.cwpitts.duckdns.org")
  (setq hass-apikey cwpitts/hass-api-key)
  (setq hass-port 443)
  (setq hass-dash-layouts
        '((default . ((hass-dash-group
                       :title "Home Assistant"
                       :format "%t\n\n%v"
                       (hass-dash-group
                        :title "Family Room"
                        :title-face outline-2
                        (hass-dash-toggle
                         :entity-id "light.hue_color_lamp_1"
                         :label "Corner lamp"
                         :icon "")
                        (hass-dash-toggle
                         :entity-id "light.bulb_1"
                         :label "Door lamp"
                         :icon ""))
                       (hass-dash-group
                        :title "Master Bedroom"
                        :title-face outline-2
                        (hass-dash-toggle
                         :entity-id "light.master_bedroom"
                         :label "Master bedroom lamp"
                         :icon ""))
                       (hass-dash-group
                        :title "People"
                        :title-face outline-2
                        (hass-dash-state
                         :entity-id "person.chris"
                         :name "Chris"
                         :icon "")
                        (hass-dash-state
                         :entity-id "person.karoline"
                         :name "Karoline"
                         :icon ""))))))))

;; IELM enhancements
;; https://www.n16f.net/blog/making-ielm-more-comfortable/
(defun g-ielm-init-history ()
  (let ((path (expand-file-name "ielm/history" user-emacs-directory)))
    (make-directory (file-name-directory path) t)
    (setq-local comint-input-ring-file-name path))
  (setq-local comint-input-ring-size 10000)
  (setq-local comint-input-ignoredups t)
  (comint-read-input-ring))

(defun g-ielm-write-history (&rest _args)
  (with-file-modes #o600
    (comint-write-input-ring)))

(use-package ielm
  :config
  (add-hook 'ielm-mode-hook 'eldoc-mode)
  (add-hook 'ielm-mode-hook 'g-ielm-init-history)
  (advice-add 'ielm-send-input :after 'g-ielm-write-history)
  :bind
  (:map inferior-emacs-lisp-mode-map
        ("C-l" . 'comint-clear-buffer)
        ("C-r" . 'helm-comint-input-ring)))

(use-package languagetool
  :commands (languagetool-check
             languagetool-clear-suggestions
             languagetool-correct-at-point
             languagetool-correct-buffer
             languagetool-set-language
             languagetool-server-mode
             languagetool-server-start
             languagetool-server-stop)
  :config
  (setq languagetool-java-arguments '("-Dfile.encoding=UTF-8")
        languagetool-console-command "~/.languagetool/languagetool-commandline.jar"
        languagetool-server-command "~/.languagetool/languagetool-server.jar"))

;; Visual settings
;; No toolbar
(tool-bar-mode -1)
;; Electric pair mode (automatch parens and quotes)
(electric-pair-mode 1)
;; Line/column numbering
(global-display-line-numbers-mode 1)
(column-number-mode)
(global-hl-line-mode t)
;; Startup screen customization
(setq inhibit-startup-screen t)
(setq initial-scratch-message ";; This buffer is for text that is not saved, and for Lisp evaluation.
;; To create a file, visit it with C-x C-f and enter text in its buffer.
")
(when (executable-find "fortune")
   (setq initial-scratch-message
         (with-temp-buffer
           (shell-command "fortune ~/.fortunes/scifi.fortunes" t)
           (let ((comment-start ";;"))
             (comment-region (point-min) (point-max)))
           (concat (buffer-string) "\n"))))
;; Maximize on startup
(add-to-list 'default-frame-alist '(fullscreen . maximized))
(setq-default tab-width 2)
(setq-default indent-tabs-mode nil)
(setq make-backup-files nil)
(set-face-attribute 'default nil
                    :family "Inconsolata Regular"
                    :height 120
                    :weight 'normal
                    :width 'normal)
(show-paren-mode)
;; end visual settings

(provide '.emacs)
;;; dot-emacs.el ends here
