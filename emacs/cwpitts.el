(defun cwpitts/bootstrap ()
  "Bootstrap selected packages."
  (interactive)
  (package-refresh-contents)
  (package-install-selected-packages t)
  (load-file emacs-config-path))

(defun cwpitts/first-in-tree (filename leaf)
  "Find the first occurrence of FILENAME in tree above LEAF."
  (let ((current-node leaf))
    (while (and (not (equal current-node "/")) (not (file-exists-p (expand-file-name filename current-node))))
      (setq current-node (file-name-directory (directory-file-name current-node)))
      (message (format "Current node is %s" current-node)))
    (expand-file-name filename current-node)))

(defun cwpitts/neotree--fit-window (type path c)
  "Resize neotree window to fit contents based on TYPE, with PATH and C as unused variables as far as this function is concerned."
  (if (eq type 'directory)
      (neo-buffer--with-resizable-window
       (let ((fit-window-to-buffer-horizontally t))
         (fit-window-to-buffer)))))
