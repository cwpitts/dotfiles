# Configuration Files

Various configuration ("dot") files. Currently included:

- [Emacs](https://www.gnu.org/software/emacs/)
- [BASH](https://www.gnu.org/software/bash/)
  - Git completion
  - [HSTR](https://github.com/dvorka/hstr)
  - Aliases
  - Environment variables
- [Python](https://www.python.org/)
- [ZSH](https://www.zsh.org/)
  - [P10K](https://github.com/romkatv/powerlevel10k)
  - [oh-my-zsh](https://ohmyz.sh/)
- [Git](https://git-scm.com/)
- [Clang](https://clang.llvm.org/)
  - [clang-format](https://clang.llvm.org/docs/ClangFormat.html)
