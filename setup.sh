#!/bin/bash
# Setup script to symlink all of the config files to the right places.
# Author: C. W. Pitts

# Bash
if [[ ! -f ${HOME}/.bashrc ]]
then
		dir=$(realpath bash)
		ln -s ${dir}/dot-bashrc.sh ${HOME}/.bashrc
		ln -s ${dir}/dot-bash_aliases.sh ${HOME}/.bash_aliases
		ln -s ${dir}/dot-bash_env.sh ${HOME}/.bash_env
		ln -s ${dir}/dot-ps1.sh ${HOME}/.ps1
		ln -s ${dir}/git-completion.sh ${HOME}/.git-completion.sh
		ln -s ${dir}/git-prompt.sh ${HOME}/.git-prompt.sh
fi

# Emacs
if [[ ! -f ${HOME}/.emacs ]]
then
		ln -s $(realpath emacs)/dot-emacs.el ${HOME}/.emacs
fi

# Python
if [[ ! -f ${HOME}/.pythonrc ]]
then
		ln -s $(realpath python)/dot-pythonrc.py ${HOME}/.pythonrc
fi
